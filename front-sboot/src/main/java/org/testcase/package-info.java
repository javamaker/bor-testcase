/*
 * package-info.java
 * Copyright 2017 Ilya Yushin
 */
/**
 * Application root package, declared main type and root configuration.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
package org.testcase;