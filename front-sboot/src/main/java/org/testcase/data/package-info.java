/*
 * package-info.java
 * Copyright 2017 Ilya Yushin
 */
/**
 * Contains types related to data model and validation.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
package org.testcase.data;