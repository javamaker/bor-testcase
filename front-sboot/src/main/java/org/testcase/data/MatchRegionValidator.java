/*
 * MatchRegionValidator.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.data;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

/**
 * Validates given {@link String} properties to match in a particular region.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
public final class MatchRegionValidator implements ConstraintValidator<MatchRegion, Object> {
    private static final Logger log = LoggerFactory.getLogger(MatchRegionValidator.class);

    private String first;
    private String second;
    private int firstOffset;
    private int secondOffset;
    private int limit;
    private boolean ignoreCase;

    /** {@inheritDoc} */
    @Override
    public void initialize(MatchRegion parameters) {
        this.first = parameters.first();
        this.second = parameters.second();
        this.firstOffset = parameters.firstOffset();
        this.secondOffset = parameters.secondOffset();
        this.limit = parameters.limit();
        this.ignoreCase = parameters.ignoreCase();
    }

    /**
     * Verifies that {@code value} properties identified by names {@link MatchRegion#first()} and
     * {@link MatchRegion#second()} match in a given region.
     */
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        final Class<?> type = value.getClass();

        final String firstValue = propertyValue(type, value, first);
        if (StringUtils.isEmpty(firstValue)) {
            // nothing to compare is both aren't present
            return true;
        }

        final String secondValue = propertyValue(type, value, second);
        if (StringUtils.isEmpty(secondValue)) {
            // nothing to compare is both aren't present
            return true;
        }

        // now test for given regions to have the same content:
        return firstValue.regionMatches(ignoreCase, firstOffset, secondValue, secondOffset, limit);
    }

    /**
     * Extract property {@link String} value using reflection.
     * 
     * @param type {@link Class} object for a given {@code instance}
     * @param instance bean instance to validate
     * @param propertyName name of the property to read the value for
     * @return resulting value or {@code null} if property could not be read
     */
    private static String propertyValue(Class<?> type, Object instance, String propertyName) {
        if (StringUtils.isEmpty(propertyName)) {
            log.warn("Empty property name specified for type '{}'.", type.getName());
            return null;
        }

        try {
            final PropertyDescriptor info = BeanUtils.getPropertyDescriptor(type, propertyName);
            if (info == null) {
                log.info("Ignoring missing property '{}' for type '{}'.", //
                    propertyName, type.getName());
                return null;
            }

            final Method getter = info.getReadMethod();
            if (getter == null) {
                log.info("Ignoring unreadable property '{}' for type '{}'.", //
                    propertyName, type.getName());
                return null;
            }

            final Object value = getter.invoke(instance);
            return value != null ? value.toString() : null;

        } catch (final Exception e) {
            log.warn(String.format("Failed to lookup a property '%s' for type '%s'.", //
                propertyName, type.getName()), e);
            return null;
        }
    }
}
