/*
 * MatchRegions.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.data;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Container for the repeatable {@link MatchRegion} declarations.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Documented
@Retention(RUNTIME)
@Target({ TYPE })
public @interface MatchRegions {
    MatchRegion[] value();
}
