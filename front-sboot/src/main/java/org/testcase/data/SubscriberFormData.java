/*
 * SubscriberFormData.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.data;

import java.io.Serializable;

import javax.annotation.Generated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.testcase.model.Subscriber;
import org.testcase.util.Json;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Holds form data form a subscriber record.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@MatchRegion(first = "workPhone", second = "mobilePhone", limit = 3, message = "{subscriber.phoneCodes}")
public final class SubscriberFormData implements Serializable {
    private static final long serialVersionUID = -5663327562834145212L;

    /** Subscriber's name, mandatory field with the maximum length of 10 characters. */
    @NotNull
    @Size(min = 1, max = 10, message = "{subscriber.firstName.size}")
    private String firstName;
    /** Subscriber's surname, mandatory field with the maximum length of 20 characters. */
    @NotNull
    @Size(min = 1, max = 20, message = "{subscriber.lastName.size}")
    private String lastName;
    /** Work phone of a subscriber, mandatory field of the format {@code 000-000-00-00}. */
    @NotNull
    @Pattern(regexp = "^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$", message = "{subscriber.workPhone.pattern}")
    private String workPhone;
    /** Mobile phone number of a subscriber, optional field of the format {@code 000-000-00-00}. */
    @Pattern(regexp = "^[0-9]{3}-[0-9]{3}-[0-9]{2}-[0-9]{2}$", message = "{subscriber.mobilePhone.pattern}")
    private String mobilePhone;
    /** E-mail address of a subscriber, optional field of the format {@code account@domain}. */
    @Email(message = "{subscriber.email}")
    private String email;

    /** Calculated property for easy Unified EL expressions. */
    @JsonIgnore
    public String getWorkCode() {
        return StringUtils.substring(getWorkPhone(), 0, 3);
    }

    /** Calculated property for easy Unified EL expressions. */
    @JsonIgnore
    public String getMobileCode() {
        return StringUtils.substring(getMobilePhone(), 0, 3);
    }

    public Subscriber makeRecord() {
        final Subscriber record = new Subscriber();
        record.setFirstName(firstName);
        record.setLastName(lastName);
        record.setWorkPhone(workPhone);
        record.setMobilePhone(mobilePhone);
        record.setEmail(email);
        return record;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return Json.toJsonString(this);
    }

    @Generated("ide")
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((mobilePhone == null) ? 0 : mobilePhone.hashCode());
        result = prime * result + ((workPhone == null) ? 0 : workPhone.hashCode());
        return result;
    }

    @Generated("ide")
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SubscriberFormData other = (SubscriberFormData) obj;
        if (email == null) {
            if (other.email != null) {
                return false;
            }
        } else if (!email.equals(other.email)) {
            return false;
        }
        if (firstName == null) {
            if (other.firstName != null) {
                return false;
            }
        } else if (!firstName.equals(other.firstName)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        if (mobilePhone == null) {
            if (other.mobilePhone != null) {
                return false;
            }
        } else if (!mobilePhone.equals(other.mobilePhone)) {
            return false;
        }
        if (workPhone == null) {
            if (other.workPhone != null) {
                return false;
            }
        } else if (!workPhone.equals(other.workPhone)) {
            return false;
        }
        return true;
    }
}
