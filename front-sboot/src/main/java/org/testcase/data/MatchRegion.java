/*
 * MatchRegion.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.data;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Given {@link String} properties of the annotated element must match within a given region.
 * <p/>
 * Accepts {@code String}. {@code null} elements are considered valid.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Documented
@Repeatable(MatchRegions.class)
@Constraint(validatedBy = MatchRegionValidator.class)
@Retention(RUNTIME)
@Target({ TYPE })
public @interface MatchRegion {
    /** Validation error message. */
    String message() default "{org.testcase.data.MatchRegion.message}";

    /** Ignored. */
    Class<?>[] groups() default {};

    /** Ignored. */
    Class<? extends Payload>[] payload() default {};

    /** Name of the first property. */
    String first();

    /** Name of the second property. */
    String second();

    /** Offset of the first property's value region to compare. */
    int firstOffset() default 0;

    /** Offset of the second property's value region to compare. */
    int secondOffset() default 0;

    /** Size of a region to compare. */
    int limit() default -1;

    /** Determines whether or not case should be ignored during comparison. */
    boolean ignoreCase() default false;
}
