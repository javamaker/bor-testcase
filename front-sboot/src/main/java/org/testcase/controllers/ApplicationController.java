/*
 * ApplicationController.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Top MVC controller of the application.
 * <p/>
 * Redirects any requests to {@code /home} path.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Controller
public class ApplicationController {
    /** Always returns {@code "redirect:/home"} for any request. */
    @GetMapping(path = "*")
    public String home() {
        return "redirect:/home";
    }
}
