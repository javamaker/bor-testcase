/*
 * package-info.java
 * Copyright 2017 Ilya Yushin
 */
/**
 * Spring MVC controllers.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
package org.testcase.controllers;