/*
 * SubscriberController.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.controllers;

import java.util.Formatter;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.testcase.StoreException;
import org.testcase.data.SubscriberFormData;
import org.testcase.model.Subscriber;
import org.testcase.service.RecordStore;

/**
 * Implements operations subscribers like adding new records and sending them to further processing
 * phase.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Controller
public class SubscriberController {
    private static final Logger log = LoggerFactory.getLogger(SubscriberController.class);

    /** Underlying subscriber records store. */
    @Autowired
    private RecordStore<Subscriber> store;

    /** Entry operations, displays initial add form. */
    @GetMapping(path = "/home")
    public String displayForm(SubscriberFormData subscriber, Model model) throws StoreException {
        // add variables required by the template to render:
        model.addAttribute("subscriber", subscriber);
        model.addAttribute("queue", store.enqueuedRecords());
        return "home";
    }

    /** Validates and submits a new subscriber record. */
    @PostMapping(path = "/enqueue")
    public String submitForm(//
        @Valid @ModelAttribute("subscriber") SubscriberFormData subscriber, //
        BindingResult result, Model model) throws StoreException {

        if (log.isDebugEnabled()) {
            log.debug("Request to enqueue the record: {}.", subscriber);
        }

        // check validation result:
        if (result.hasErrors()) {
            // template will render the errors, we simply log the information:
            if (log.isDebugEnabled()) {
                try (final Formatter f = new Formatter()) {
                    f.format("Validator returned errors for a given record (%s):", //
                        result.getObjectName());

                    // we have both, global and field errors;
                    // let's print them all to logs:
                    final List<ObjectError> globalErrors = result.getGlobalErrors();
                    globalErrors.forEach((e) -> f.format("%n    %-15s: %s", //
                        result.getObjectName(), e.getDefaultMessage()));

                    final List<FieldError> fieldErrors = result.getFieldErrors();
                    fieldErrors.forEach((e) -> f.format("%n    %-15s: %s", //
                        e.getField(), e.getDefaultMessage()));

                    f.flush();
                    log.debug(f.toString());
                }
            } else if (log.isInfoEnabled()) {
                log.info("Validator returned {} errors for a given record ({})", //
                    result.getErrorCount(), result.getObjectName());
            }
        } else {
            log.info("Record validation successful.");

            // add the record to the queue and reset the form data:
            store.enqueueRecord(subscriber.makeRecord());
            model.addAttribute("subscriber", new SubscriberFormData());
        }

        // add validation results required by the template to render:
        model.addAttribute("queue", store.enqueuedRecords());

        return "home";
    }

    /** Sends collected records queue to further processing. */
    @PostMapping(path = "/commit")
    public String commitForm(SubscriberFormData subscriber, BindingResult result, Model model)
        throws StoreException {

        if (log.isDebugEnabled()) {
            log.debug("Request to process the queue.");
        }

        // send the collected queue to further phase:
        try {
            store.commit();

        } catch (final Exception e) {
            log.error("", e);
        }

        // add variables required by the template to render:
        model.addAttribute("subscriber", subscriber);
        model.addAttribute("queue", store.enqueuedRecords());

        return "home";
    }
}
