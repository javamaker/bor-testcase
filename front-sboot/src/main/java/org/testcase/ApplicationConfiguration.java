/*
 * ApplicationConfiguration.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.testcase.model.Subscriber;
import org.testcase.service.JsonDirectoryLayout;
import org.testcase.service.JsonDirectoryStore;
import org.testcase.service.RecordStore;
import org.testcase.util.Json;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Root application configuration class.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Configuration
public class ApplicationConfiguration extends WebMvcConfigurerAdapter {
    /** Loads {@code i18n} message bundle. */
    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        final ReloadableResourceBundleMessageSource source = new ReloadableResourceBundleMessageSource();
        source.setBasename("classpath:messages");
        source.setCacheSeconds(60);
        return source;
    }

    /** Configures {@code i18n} switch for a user session. */
    @Bean
    public LocaleResolver localeResolver() {
        final SessionLocaleResolver resolver = new SessionLocaleResolver();
        resolver.setDefaultLocale(Locale.US);
        return resolver;
    }

    /** Configures intercepter that changes locale on new query parameter {@code lang}. */
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        final LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
        interceptor.setParamName("lang");
        return interceptor;
    }

    /** Registers intercepter from {@link LocaleChangeInterceptor}. */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(localeChangeInterceptor());
    }

    /** Configures thread executor to process a queue of storage operations. */
    @Bean
    public ExecutorService executor() {
        return Executors.newSingleThreadExecutor();
    }

    /** Jackson object mapper used in this environment. */
    @Bean
    public ObjectMapper objectMapper() {
        return Json.createDefaultMapper();
    }

    /** A bean that defines a type of records of the {@link #store()}. */
    @Bean
    @Qualifier("recordType")
    public Class<Subscriber> recordType() {
        return Subscriber.class;
    }

    /** A bean that manages a directory layout for the {@link #store()}. */
    @Bean
    public JsonDirectoryLayout<Subscriber> directoryLayout() {
        return new JsonDirectoryLayout<>();
    }

    /** Configures JSON file storage for {@link Subscriber} records. */
    @Bean
    public RecordStore<Subscriber> store() {
        return new JsonDirectoryStore<>();
    }
}
