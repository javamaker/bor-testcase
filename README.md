## Getting Started

#### Prerequisites

In order to install the software you'll need:

* Java SE Development Kit 8 ([ available here ](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)) 
* Apache Maven 3.3.9 ([ available here ](https://maven.apache.org/download.cgi))


#### Installing

Clone project source:

```
#!shell

  git clone https://bitbucket.org/javamaker/bor-testcase
```

Go to the working copy directory:

```
#!shell

  cd bor-testcase
```

Run build script:

```
#!shell
  mvn clean package
```


#### Running

Run frontend application:

```
#!shell

java -jar front-sboot\target\front-sboot-application.jar
```

Run backend application:

```
#!shell

java -jar back-camel\target\back-camel-application.jar

```

## Project Structure

 Module          | Description                                     
 --------------- | ----------------------------------------------- 
 `./`            | Maven aggregator POM                            
 `parent`        | Maven parent POM                                
 `back-camel`    | The backend module (Apache Camel + Spring Boot) 
 `front-sboot`   | The frontend module (Spring Boot Web)           
 `services`      | The module with shared services and classes