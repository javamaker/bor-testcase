/*
 * EnrichmentStrategy.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.routes;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.stereotype.Component;
import org.testcase.model.EnrichedSubscriber;
import org.testcase.model.Subscriber;

/**
 * Aggregation strategy used to merge input JSON data with enrichment.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Component("enrichment")
public class EnrichmentStrategy implements AggregationStrategy {
    /**
     * Converts input {@link Subscriber} objects to {@link EnrichedSubscriber}'s.
     */
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        final Message oldMessage = oldExchange.getIn();
        final Message newMessage = newExchange.getIn();

        if (oldMessage != null && newMessage != null) {
            final Object subscriber = oldMessage.getBody();
            final Object work = newMessage.getBody();
            if (subscriber instanceof EnrichedSubscriber && work != null) {
                ((EnrichedSubscriber) subscriber).setWork(work.toString());
            }
        }

        return oldExchange;
    }
}
