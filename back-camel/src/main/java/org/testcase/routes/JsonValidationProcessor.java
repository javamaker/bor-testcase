/*
 * JsonValidationProcessor.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.routes;

import java.io.FilterReader;
import java.io.Reader;

import org.apache.camel.AsyncCallback;
import org.apache.camel.AsyncProcessor;
import org.apache.camel.Exchange;
import org.apache.camel.util.AsyncProcessorHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.testcase.model.Subscriber;
import org.testcase.service.JsonValidator;

/**
 * Camel processor implementation that validates data against {@link Subscriber} JSON schema.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Component("validator")
public class JsonValidationProcessor implements AsyncProcessor {
    /** The underlying JSON validator bound to {@link Subscriber} schema. */
    @Autowired
    private JsonValidator validator;

    /** Delegates call to {@link #process(Exchange, AsyncCallback)} synchronously. */
    @Override
    public void process(Exchange exchange) throws Exception {
        AsyncProcessorHelper.process(this, exchange);
    }

    /**
     * Applies validator to the {@code exchange} body.
     */
    @Override
    public boolean process(Exchange exchange, AsyncCallback callback) {
        try {
            doProcess(exchange);
        } catch (final Exception e) {
            exchange.setException(e);
        }
        callback.done(true);
        return true;
    }

    /** Calls the underlying validator for a given {@code exchange} body. */
    private void doProcess(Exchange exchange) throws Exception {
        final Reader dataInput = dataInput(exchange);
        validator.validate(dataInput);
    }

    /** Creates a {@link Reader} for given {@code exchange} body. */
    protected Reader dataInput(Exchange exchange) {
        return new NamedReader(exchange, exchange.getIn().getBody(Reader.class));
    }

    /**
     * {@link Reader} wrapper that keeps the name of original files. Used locate errors within s
     * source file.
     * 
     * @author Ilya Yushin
     * @version $Id$
     */
    private static final class NamedReader extends FilterReader {
        private final String filename;

        public NamedReader(Exchange exchange, Reader in) {
            super(in);
            filename = exchange.getIn().getHeader("CamelFileNameOnly", String.class);
        }

        @Override
        public String toString() {
            return filename;
        }
    }
}
