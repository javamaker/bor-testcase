/*
 * RouteDirectoryLayout.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.routes;

import java.io.IOException;
import java.nio.file.Path;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.testcase.service.JsonDirectoryLayout;

/**
 * Extends {@link JsonDirectoryLayout} by adding the output directory.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Component
public class RouteDirectoryLayout<T> extends JsonDirectoryLayout<T> {
    private static final Logger log = LoggerFactory.getLogger(RouteDirectoryLayout.class);
    
    /** Configuration prefix for the bean. */
    public static final String CONFIGURATION_PREFIX = "processor";
    
    /** The name of the property pointing to a fully processed data directory. */
    public static final String OUTPUT_DIRECTORY_PROPERTY = CONFIGURATION_PREFIX + ".outputDir";
    /** The name of the property pointing to a directory where to put invalid JSON data. */
    public static final String INVALID_DIRECTORY_PROPERTY = CONFIGURATION_PREFIX + ".invalidDir";
    /** The name of the property pointing to a failed to process data directory. */
    public static final String ERROR_DIRECTORY_PROPERTY = CONFIGURATION_PREFIX + ".failedDir";

    /** Path of a fully processed data directory. */
    private Path outputDir;
    /** Path of a directory where failed data is moved to. */
    private Path errorDir;
    /** Path of a directory where invalid JSON data is move to. */
    private Path invalidDir;

    public String getInputDirectory() {
        return processDir().toString();
    }

    public String getOutputDirectory() {
        return outputDir.toString();
    }

    public String getInvalidDirectory() {
        return invalidDir.toString();
    }

    public String getErrorDirectory() {
        return errorDir.toString();
    }

    public boolean makeDirectories() {
        return makeDirs();
    }

    /**
     * Overrides parent function to configure additional output directory.
     */
    @Override
    @PostConstruct
    public void configureLayout() throws IOException {
        super.configureLayout();

        outputDir = configureDirectory(OUTPUT_DIRECTORY_PROPERTY, "data");
        log.info("Output directory path: {}", outputDir.toString());

        invalidDir = configureDirectory(INVALID_DIRECTORY_PROPERTY, "invalid");
        log.info("Invalid directory path: {}", outputDir.toString());

        errorDir = configureDirectory(ERROR_DIRECTORY_PROPERTY, "failed");
        log.info("Failed directory path: {}", outputDir.toString());
    }
}
