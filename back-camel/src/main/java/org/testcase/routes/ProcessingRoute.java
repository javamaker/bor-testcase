/*
 * ProcessingRoute.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.routes;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.Formatter;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.testcase.ValidationException;

/**
 * Route builder for the main application processing flow.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Component("processor")
public class ProcessingRoute<T extends Serializable> extends RouteBuilder {
    private static final Logger log = LoggerFactory.getLogger(ProcessingRoute.class);

    /** A bean managing working directory layout. */
    @Autowired
    private RouteDirectoryLayout<T> layout;
    /** Aggregation strategy used to merge input data with enrichment. */
    @Autowired
    private EnrichmentStrategy enricher;
    /** Initial delay in milliseconds for the input polling consumer. */
    @Value("${" + RouteDirectoryLayout.CONFIGURATION_PREFIX + ".initialDelay:100}")
    private int initialDelay;
    /** Polling consumer timeout in milliseconds. */
    @Value("${" + RouteDirectoryLayout.CONFIGURATION_PREFIX + ".delay:1000}")
    private int delay;
    /** Indicates whether or not the process should delete input files. */
    @Value("${" + RouteDirectoryLayout.CONFIGURATION_PREFIX + ".deleteInput:false}")
    private boolean deleteInput;

    /**
     * Configures and registers routes of the application.
     */
    @Override
    public void configure() throws Exception {
        // 1. Dispatch input:
        from(inputUri()) //
            .routeId("input") //
            .log("Found input \"${file:name}\" at \"${file:absolute.path}\".") //
            .to("direct:validate") //
            .end();

        // 2. Validation:
        from("direct:validate") //
            .routeId("validate") //
            .log("Validating \"${file:name}\" against JSON schema.") //
            .doTry() //
            .process("validator") //
            .log("File \"${file:name}\" is VALID.") //
            .to("direct:enrich-splitter") //
            .to("direct:save") //
            .doCatch(ValidationException.class) //
            .log("File \"${file:name}\" is INVALID: ${exception.message}.") //
            .log("${exception.description}") //
            .to(exceptionUri()) //
            .to(invalidUri()) //
            .log("File \"${file:name}\" was moved to \"${header.CamelFileNameProduced}\".") //
            .setBody(simple("${exception.description}")) //
            .to(invalidUri("fileName=${header.CamelFileName}.errors")) //
            .doCatch(Throwable.class) //
            .to(exceptionUri()) //
            .to(deadLetterChannel(failedUri()).disableRedelivery().getDeadLetter()) //
            .setBody(simple("${exception.description}")) //
            .to(failedUri("fileName=${header.CamelFileName}.errors")) //
            .log("Failure for \"${file:name}\" is written to \"${header.CamelFileNameProduced}\".") //
            .end();

        // 3. Enrichment split and aggregate:
        from("direct:enrich-splitter") //
            .routeId("enrich-splitter") //
            .log("Starting enrichment for \"${file:name}\".") //
            .unmarshal("inputDataFormat") //
            .split(body()) //
            .to("direct:enrich-item") //
            .to("direct:enrich-aggregate") //
            .end();

        // 3.1 Enrich single record
        from("direct:enrich-item") //
            .routeId("enrich-item") //
            .enrich(enrichUri(), enricher) //
            .log("\"${file:name}[${header.CamelSplitIndex}]\" enrichment result: \"${body}\".") //
            .end();

        // 3.2 Enrich single record
        from("direct:enrich-aggregate") //
            .routeId("enrich-aggregate") //
            .aggregate(header("CamelSplitIndex"), new SubscriberAggregationStrategy()) //
            .completionSize(simple("${header.CamelSplitSize}")) //
            .log("Enrichment for \"${file:name}\" is complete.") //
            .end();

        // 4. Save result:
        from("direct:save") //
            .routeId("save") //
            .marshal("outputDataFormat") //
            .to(outputUri()) //
            .log("Result for \"${file:name}\" is written to \"${header.CamelFileNameProduced}\".") //
            .to(loggerUri("showExchangePattern=false", "multiline=true")) //
            .end();
    }

    protected String enrichUri() {
        try (Formatter f = new Formatter()) {
            f.format("sql:classpath:%s", enrichQueryScript());
            f.format("?dataSource=%s", "dataSource");
            f.format("&outputType=%s", "SelectOne");
            f.flush();
            return f.toString();
        }
    }

    /** Returns {@code file} URI for the input consumer. */
    protected String inputUri() {
        try (Formatter f = new Formatter()) {
            f.format("file:%s", layout.getInputDirectory());
            f.format("?autoCreate=%s", layout.makeDirectories());
            f.format("&include=%s", "^json_\\d{6}_\\d{4}\\.txt$");
            f.format("&delete=%s", deleteInput);
            f.format("&initialDelay=%d", initialDelay);
            f.format("&delay=%d", delay);
            f.format("&doneFileName=%s", "${file:name.noext}.ready");
            f.flush();
            return f.toString();
        }
    }

    /** Returns {@code file} URI for the valid output producer. */
    protected String outputUri() {
        return outputUri(layout.getOutputDirectory());
    }

    /** Returns {@code file} URI for the invalid output producer. */
    protected String invalidUri(String... extra) {
        return outputUri(layout.getInvalidDirectory(), extra);
    }

    /** Returns {@code file} URI for the producer of the data failed to process. */
    protected String failedUri(String... extra) {
        return outputUri(layout.getErrorDirectory(), extra);
    }

    /**
     * Formats {@code file} URI for a given output directory.
     * 
     * @param layoutPath name of the output
     * @param extra additional URI parameters to append
     * @return resulting URI string
     */
    protected String outputUri(String layoutPath, String... extra) {
        try (Formatter f = new Formatter()) {
            f.format("file:%s", layoutPath);
            f.format("?charset=%s", charset());
            for (int i = 0; i < extra.length; i++) {
                f.format("&%s", extra[i]);
            }
            f.flush();
            return f.toString();
        }
    }

    /**
     * Formats {@code log} URI with given parameters.
     * 
     * @param parameters URI parameters to append
     * @return resulting URI string
     */
    protected String loggerUri(String... parameters) {
        try (Formatter f = new Formatter()) {
            f.format("log:%s", log.getName());
            for (int i = 0; i < parameters.length; i++) {
                f.format(i == 0 ? "?%s" : "&%s", parameters[i]);
            }
            f.flush();
            return f.toString();
        }
    }

    /** Returns {@code log} URI to use for error reporting. */
    protected String exceptionUri() {
        try (Formatter f = new Formatter()) {
            f.format("log:%s", log.getName());
            f.format("?showException=%s", true);
            f.format("&showCaughtException=%s", true);
            f.format("&showStackTrace=%s", log.isDebugEnabled());
            f.flush();
            return f.toString();
        }
    }

    /**
     * Returns a path of the enrichment query script.
     * 
     * @return enrichment query script
     */
    protected String enrichQueryScript() {
        return "datastore/enrich-query.sql";
    }

    /**
     * Returns the character encoding to use for output.
     * 
     * @return output charset
     */
    protected String charset() {
        return StandardCharsets.UTF_8.name();
    }
}
