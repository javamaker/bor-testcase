/*
 * ListAggregationStrategy.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.routes;

import java.io.Serializable;
import java.util.ArrayList;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcase.model.Subscriber;

/**
 * Collects back enriched {@link Subscriber} records after splitter.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
public class SubscriberAggregationStrategy implements AggregationStrategy, Serializable {
    private static final Logger log = LoggerFactory.getLogger(SubscriberAggregationStrategy.class);
    private static final long serialVersionUID = 5167891733430355102L;

    /** Merges the bodies of given exchange objects to list inside {@code newExchange}'s body. */
    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        final Subscriber item = inBody(newExchange, Subscriber.class);
        if (item == null) {
            return newExchange;
        }

        final SubscriberList list;
        final Exchange result;
        if (oldExchange == null) {
            if (log.isDebugEnabled()) {
                log.debug("Aggregate(old = null, new = {})", item);
            }

            list = new SubscriberList();
            result = newExchange;

        } else {
            list = inBody(oldExchange, SubscriberList.class);
            result = newExchange;

            if (log.isDebugEnabled()) {
                log.debug("Aggregate(old = {}, new = {})", list, item);
            }
        }

        list.add(item);
        result.getIn().setBody(list);
        return result;
    }

    protected <T> T inBody(Exchange exchange, Class<T> bodyType) {
        if (exchange == null) {
            return null;
        }

        final Message in = exchange.getIn();
        if (in == null) {
            return null;
        }

        final Object body = in.getBody();
        if (body == null) {
            return null;
        }

        return bodyType.cast(body);
    }

    protected static class SubscriberList extends ArrayList<Subscriber> {
        private static final long serialVersionUID = -3588305476319388077L;
    }
}
