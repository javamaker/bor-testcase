/*
 * EnrichedSubscriber.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.model;

/**
 * Extends subscriber DTO with enrichment data.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
public class EnrichedSubscriber extends Subscriber {
    private static final long serialVersionUID = -4886853247044128166L;

    /** The work field enriched from a local datasource. */
    private String work;

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }
}
