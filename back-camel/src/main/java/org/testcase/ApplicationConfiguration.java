/*
 * ApplicationConfiguration.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase;

import org.apache.camel.component.jackson.JacksonDataFormat;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.testcase.model.EnrichedSubscriber;
import org.testcase.model.Subscriber;
import org.testcase.service.JsonValidator;
import org.testcase.util.Json;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Root application configuration class.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@Configuration
public class ApplicationConfiguration {

    /** A bean that defines a type of records of the {@link #store()}. */
    @Bean
    @Qualifier("recordType")
    public Class<Subscriber> recordType() {
        return Subscriber.class;
    }

    /** Jackson object mapper used in this environment. */
    @Bean
    public ObjectMapper objectMapper() {
        return Json.createDefaultMapper();
    }

    /** Jackson deserializer metadata for input files. */
    @Bean
    public JacksonDataFormat inputDataFormat() {
        final JacksonDataFormat format = new JacksonDataFormat( //
            EnrichedSubscriber.class, Subscriber.class, false);
        format.setUseList(true);
        format.setObjectMapper(objectMapper());
        return format;
    }

    /** Jackson serializer metadata for output files. */
    @Bean
    public JacksonDataFormat outputDataFormat() {
        final Class<EnrichedSubscriber> type = EnrichedSubscriber.class;
        final JacksonDataFormat format = new JacksonDataFormat(type, type, false);
        format.setUseList(true);
        format.setObjectMapper(objectMapper());
        return format;
    }

    /** Jackson schema validator. */
    @Bean
    public JsonValidator jsonValidator() {
        return new JsonValidator();
    }
}
