/*
 * Application.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Application launcher.
 * <p>
 * Starts a new instance of the Camel router.
 * </p>
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
@SpringBootApplication
public class Application {
    /**
     * Main function.
     * 
     * @param args ignored
     */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
