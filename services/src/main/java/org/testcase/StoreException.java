/*
 * StoreException.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase;

import org.testcase.service.RecordStore;

/**
 * Exception thrown by {@link RecordStore} imeplementation to indicates that there is something
 * wrong happens.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
public final class StoreException extends Exception {
    private static final long serialVersionUID = 6838729526037902232L;

    /**
     * Creates a new exception with a given {@code message} and given {@code cause}.
     * 
     * @param message a message with detail information
     * @param cause exception caused this one
     */
    public StoreException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Creates a new exception with a given {@code message}.
     * 
     * @param message a message with detail information
     */
    public StoreException(String message) {
        super(message);
    }
}
