/*
 * package-info.java
 * Copyright 2017 Ilya Yushin
 */
/**
 * Contains common types and module configuration.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
package org.testcase;