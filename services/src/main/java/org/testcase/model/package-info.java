/*
 * package-info.java
 * Copyright 2017 BPC Group Banking Technologies
 */
/**
 * Data model types used by the services.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
package org.testcase.model;