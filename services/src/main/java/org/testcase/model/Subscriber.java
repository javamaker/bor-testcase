/*
 * 
 * Subscriber.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.model;

import java.io.Serializable;

import javax.annotation.Generated;

import org.testcase.util.Json;

/**
 * Plain DTO that keeps information about a single phone book subscriber.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
public class Subscriber implements Serializable {
    private static final long serialVersionUID = 1997364771158677482L;

    /** Subscriber's name. */
    private String firstName;
    /** Subscriber's surname. */
    private String lastName;
    /** Work phone of a subscriber. */
    private String workPhone;
    /** Mobile phone number of a subscriber. */
    private String mobilePhone;
    /** E-mail address of a subscriber. */
    private String email;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return Json.toJsonString(this);
    }

    @Generated("ide")
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
        result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
        result = prime * result + ((mobilePhone == null) ? 0 : mobilePhone.hashCode());
        result = prime * result + ((workPhone == null) ? 0 : workPhone.hashCode());
        return result;
    }

    @Generated("ide")
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Subscriber other = (Subscriber) obj;
        if (email == null) {
            if (other.email != null) {
                return false;
            }
        } else if (!email.equals(other.email)) {
            return false;
        }
        if (firstName == null) {
            if (other.firstName != null) {
                return false;
            }
        } else if (!firstName.equals(other.firstName)) {
            return false;
        }
        if (lastName == null) {
            if (other.lastName != null) {
                return false;
            }
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }
        if (mobilePhone == null) {
            if (other.mobilePhone != null) {
                return false;
            }
        } else if (!mobilePhone.equals(other.mobilePhone)) {
            return false;
        }
        if (workPhone == null) {
            if (other.workPhone != null) {
                return false;
            }
        } else if (!workPhone.equals(other.workPhone)) {
            return false;
        }
        return true;
    }
}
