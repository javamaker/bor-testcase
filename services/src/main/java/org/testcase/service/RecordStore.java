/*
 * SubscriberStore.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.service;

import java.io.Serializable;
import java.util.List;

import org.testcase.StoreException;

/**
 * Back store interface.
 * <p/>
 * The store's concept implies two phases of data processing. First, records are collected into a
 * buffer queue using method {@link #enqueueRecord(Serializable)}. Once the queue contains all
 * required data and is ready for further processing it is being sent using {@link #commit()}.
 * <p/>
 * Implementation should store data and guarantee its persistence in both enqueued and committed
 * states. Committed is virtually send out of {@link RecordStore} and becomes unavailable for
 * further management.
 * 
 * 
 * @author Ilya Yushin
 * @version $Id$
 * 
 * @param <T> type of data records the store implementation is intended for
 */
public interface RecordStore<T extends Serializable> {
    /**
     * Adds a given record the buffer queue.
     * 
     * @param record a record to enqueue
     * @throws StoreException if enqueue failed due to error
     */
    void enqueueRecord(T record) throws StoreException;

    /**
     * Returns current size of the queue.
     * 
     * @return queue size
     * @throws StoreException if queue access failed
     */
    int queueSize() throws StoreException;

    /**
     * Extract all records currently being buffered in the queue.f
     * 
     * @return queue contents
     * @throws StoreException if queue access failed
     */
    List<T> enqueuedRecords() throws StoreException;

    /**
     * Flushes the entire queue to a commit location.
     * 
     * @throws StoreException if queue commit failed due to some reason
     */
    void commit() throws StoreException;
}
