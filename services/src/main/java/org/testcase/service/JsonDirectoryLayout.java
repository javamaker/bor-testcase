/*
 * JsonDirectoryLayout.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;

/**
 * Manages a directory structure for {@link JsonDirectoryStore} instances.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
public class JsonDirectoryLayout<T> {
    private static final Logger log = LoggerFactory.getLogger(JsonDirectoryLayout.class);

    /** Configuration prefix for the bean. */
    public static final String CONFIGURATION_PREFIX = "store.json";

    /** The name of the property pointing to a buffering queue directory. */
    public static final String PREPARE_DIRECTORY_PROPERTY = CONFIGURATION_PREFIX + ".prepareDir";
    /** The name of the property pointing to a committed data directory. */
    public static final String PROCESS_DIRECTORY_PROPERTY = CONFIGURATION_PREFIX + ".processDir";
    /** The property pointing to default home directory that is used for missing layout entries. */
    public static final String DEFAULT_DIRECTORY_PROPERTY = CONFIGURATION_PREFIX + ".defaultDir";
    /** The boolean property that indicates whether or not missing directories should be created. */
    public static final String CREATE_LAYOUT_PROPERTY = CONFIGURATION_PREFIX + ".makeDirs";

    /** Injects the properties of the container environment. */
    @Autowired
    private Environment env;
    /** The type of this store records, used for naming when creating a default structure. */
    @Autowired(required = false)
    @Qualifier("recordType")
    private Class<T> type;

    /** Cached value of the default home directory assigned in {@link #defaultDir()}. */
    private Path defaultDir;
    /** Cached value of the flag that indicates whether missing directories should be created. */
    private Boolean makeDirs;

    /** Path of a buffering queue directory. */
    private Path prepareDir;
    /** Path of a committed data directory. */
    private Path processDir;

    /**
     * Resolves a given {@code subPath} using the prepare directory as a parent.
     * 
     * @param subPath directory of filename to resolve
     * @return resulting path
     */
    public Path getPreparePath(CharSequence subPath) {
        Objects.requireNonNull("", "subPath must not be null");
        return prepareDir.resolve(subPath.toString());
    }

    /**
     * Resolves a given {@code subPath} using the committed data directory as a parent.
     * 
     * @param subPath directory of filename to resolve
     * @return resulting path
     */
    public Path getProcessPath(CharSequence subPath) {
        Objects.requireNonNull("", "subPath must not be null");
        return processDir.resolve(subPath.toString());
    }

    @PostConstruct
    public void configureLayout() throws IOException {
        prepareDir = configureDirectory(PREPARE_DIRECTORY_PROPERTY, "prepare");
        log.info("Prepare directory path: {}", prepareDir.toString());

        processDir = configureDirectory(PROCESS_DIRECTORY_PROPERTY, "work");
        log.info("Process directory path: {}", processDir.toString());
    }

    /**
     * Prepares layout entry for a given {@code propertyName}.
     * <p/>
     * Is the given property is not defined then this function generate a new path using default
     * home as the base and {@code defaultDirName} as directory name.
     * 
     * @param propertyName property name for the entry
     * @param defaultDirName generated directory name if {@code propertyName} has no value
     * @return resulting directory
     * @throws IOException if some of the file system operations failed
     */
    protected Path configureDirectory(String propertyName, String defaultDirName)
        throws IOException {

        Objects.requireNonNull(propertyName, "propertyName must not be null.");
        Objects.requireNonNull(propertyName, "defaultDirName must not be null.");

        final String value = env().getProperty(propertyName);
        Path path;
        if (!StringUtils.isBlank(value)) {
            final Path tmp = Paths.get(value);
            if (tmp.isAbsolute()) {
                path = tmp;
            } else {
                path = defaultDir().resolve(tmp);
            }
        } else {
            path = defaultDir().resolve(defaultDirName);
        }

        if (!Files.exists(path) && makeDirs()) {
            Files.createDirectories(path);
        }

        return path;
    }

    /**
     * Returns the injected instance of {@link Environment}.
     * 
     * @return container environment.
     */
    protected final Environment env() {
        return env;
    }

    /**
     * Returns path of the processing directory.
     * 
     * @return path of the processing directory.
     */
    protected final Path processDir() {
        return processDir;
    }

    /**
     * Returns a {@link Path} to the the default home directory creating it if necessary.
     * 
     * @return default home directory
     * @throws IOException if some of the file system operations failed
     */
    protected final Path defaultDir() throws IOException {
        // check if we already assigned the value:
        if (defaultDir != null) {
            return defaultDir;
        }

        defaultDir = configureDefaultDir();
        log.info("Default home directory path: {}.", defaultDir.toString());

        return defaultDir;
    }

    protected final boolean makeDirs() {
        // check if we already assigned the value:
        if (makeDirs != null) {
            return makeDirs.booleanValue();
        }

        makeDirs = ObjectUtils.defaultIfNull(configureMakeDirs(), Boolean.TRUE);
        log.info("Make missing directories: {}.", makeDirs.booleanValue() ? "enabled" : "disabled");

        return makeDirs.booleanValue();
    }

    /**
     * Prepares a {@link Path} to the the default home directory.
     *
     * @return default home directory
     * @throws IOException if some of the file system operations failed
     */
    protected Path configureDefaultDir() throws IOException {
        // try to extract the configured value
        final Path defaultPath;
        final String value = env().getProperty(DEFAULT_DIRECTORY_PROPERTY);
        if (!StringUtils.isBlank(value)) {
            // convert it to Path:
            defaultPath = Paths.get(value).toAbsolutePath();

        } else {
            // if no value specified then we generate a new one:
            final String homeDir = env().getRequiredProperty("user.home");
            final String workDirName = '.' + CONFIGURATION_PREFIX;
            if (type != null) {
                final String sectionName = type.getSimpleName().toLowerCase();
                defaultPath = Paths.get(homeDir, workDirName, sectionName);
            } else {
                defaultPath = Paths.get(homeDir, workDirName);
            }
        }

        // check if we are to create missing layout entries:
        if (makeDirs()) {
            Files.createDirectories(defaultPath);
        }

        return defaultPath;
    }

    /**
     * Reads configuration flag turning on or off auto creation of missing directories.
     * 
     * @return flag value or a default value if it property is not set
     */
    protected Boolean configureMakeDirs() {
        return env().getProperty(CREATE_LAYOUT_PROPERTY, Boolean.class, Boolean.TRUE);
    }
}
