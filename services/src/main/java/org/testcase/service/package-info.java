/*
 * package-info.java
 * Copyright 2017 Ilya Yushin
 */
/**
 * Contains application level services related to subscriber data processing.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
package org.testcase.service;