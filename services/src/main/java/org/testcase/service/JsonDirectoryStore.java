/*
 * JsonDirectoryStore.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testcase.StoreException;
import org.testcase.util.Json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * Implements {@link RecordStore} using JSON file store.
 * <p/>
 * This implementation serializes enqueued records as JSON value to a file {@code "jsondata.txt"}.
 * The file is located in a sub-directory {@code "prepare"} of the base location. During commit the
 * queue file is being moved to another sub-directory, {@code work}. During move the file is also
 * renamed so that its name ends with commit time print.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
public class JsonDirectoryStore<T extends Serializable> implements RecordStore<T> {
    private static final Logger log = LoggerFactory.getLogger(JsonDirectoryStore.class);

    /** Encoding user to serialize records. */
    private static final Charset CHARSET = StandardCharsets.UTF_8;

    /** Jackson mapper we'll use for object serialization. */
    @Autowired(required = false)
    private ObjectMapper mapper;
    /** The type of this store records. */
    @Autowired
    @Qualifier("recordType")
    private Class<T> type;
    /** A bean managing working directory layout. */
    @Autowired
    private JsonDirectoryLayout<T> layout;
    /** The path of a directory where committed data will be placed. */
    @Autowired
    private ExecutorService executor;
    /** Jackson type reference of this store records, used for type save deserialization. */
    private CollectionType queueType;
    /** The path of a file where enqueued records will be stored. */
    private Path queueFile;

    /**
     * Configures this store, create working sub-folders.
     * 
     * @throws IOException if file system operations fail
     */
    @PostConstruct
    public void postConstruct() throws IOException {
        // initialize Jackson serialization type for the queue file:
        this.queueType = TypeFactory.defaultInstance().constructCollectionType(List.class, type);

        // initialize a queue file, e.g. the file where enqueued records will be stored:
        this.queueFile = layout.getPreparePath("jsondata.txt");

        if (this.mapper == null) {
            this.mapper = Json.createDefaultMapper();
        }
    }

    /**
     * Returns the type records this service works with.
     * 
     * @return type of the store records
     */
    public Class<T> getType() {
        return type;
    }

    /** Appends a given record to a JSON array stored in {@code "jsondata.txt"}. */
    @Override
    public void enqueueRecord(T record) throws StoreException {
        if (record != null) {
            try {
                executeTask(() -> {
                    final List<T> queue = loadQueue();
                    queue.add(record);
                    storeQueue(queue);
                    return null;
                });
            } catch (final ExecutionException e) {
                throw new StoreException("Couldn't load enqueued records.", e);
            }
        }
    }

    /** Returns the size of the JSON array stored in {@code "jsondata.txt"}. */
    @Override
    public int queueSize() throws StoreException {
        try {
            return executeTask(this::loadQueue).size();
        } catch (final ExecutionException e) {
            throw new StoreException("Couldn't load enqueued records.", e);
        }
    }

    /** Returns the elements of the JSON array stored in {@code "jsondata.txt"}. */
    @Override
    public List<T> enqueuedRecords() throws StoreException {
        try {
            return executeTask(this::loadQueue);
        } catch (final ExecutionException e) {
            throw new StoreException("Couldn't load enqueued records.", e);
        }
    }

    /** Moves the {@code "jsondata.txt"} file from {@code "prepare"} to {@code "work"}. */
    @Override
    public void commit() throws StoreException {
        if (Files.exists(queueFile)) {
            final Path commitFile = configureCommitFile();
            final Path flagFile = configureFlagFile(commitFile);
            try {
                Files.move(queueFile, commitFile, StandardCopyOption.ATOMIC_MOVE);
                Files.createFile(flagFile);
            } catch (final IOException e) {
                throw new StoreException("Couldn't move enqueued records from " //
                    + queueFile + " to " + commitFile + ".", e);
            }
        }
    }

    /**
     * Reads all enqueued record from the Jackson back store.
     * 
     * @return a collection of records
     * @throws StoreException in case of any error interrupted the loading
     */
    private List<T> loadQueue() throws StoreException {
        if (Files.exists(queueFile)) {
            try (BufferedReader reader = Files.newBufferedReader(queueFile, CHARSET)) {
                return mapper.readValue(reader, queueType);
            } catch (final JsonProcessingException e) {
                throw new StoreException("Couldn't deserialize enqueued records.", e);
            } catch (final FileNotFoundException e) {
                log.warn("File \"{}\" has been unexpectedly moved.", queueFile);
            } catch (final IOException e) {
                throw new StoreException("Couldn't load enqueued records.", e);
            }
        }

        return new ArrayList<>();
    }

    /**
     * Writes a given {@code queue} with records to the Jackson back store.
     * 
     * @param queue a queue to store
     * @throws StoreException in case of any error interrupted the storing
     */
    private void storeQueue(List<T> queue) throws StoreException {
        try (BufferedWriter writer = Files.newBufferedWriter(queueFile, CHARSET)) {
            mapper.writeValue(writer, queue);
        } catch (final JsonProcessingException e) {
            throw new StoreException("Couldn't serialize enqueued records.", e);
        } catch (final IOException e) {
            throw new StoreException("Couldn't store enqueued records.", e);
        }
    }

    /**
     * Prepares commit target path.
     * 
     * @param commitDir commit output directory
     * @return resulting path to move the file to
     */
    private Path configureCommitFile() {
        // get the name in a format of json_ddMMyy_hhmm[.N].txt where:
        // ddMMyy - commit/replacement date
        // hhmm - commit/replacement time
        // [.N] - optional fragment, the ordinal used to prevent file replacement
        final String name = String.format("json_%1$td%1$tm%1$ty_%1$tH%1$tM", new Date());
        Path file = layout.getProcessPath(name + ".txt");
        if (!Files.exists(file)) {
            return file;
        }

        // if file exists we increment ordinal and append suffix until the name is vacant:
        final StringBuilder buffer = new StringBuilder(name);
        final int nameLength = name.length();
        for (int i = 1;; ++i) {
            buffer.setLength(nameLength);
            buffer.append(".").append(i).append(".txt");
            file = layout.getProcessPath(buffer);
            if (!Files.exists(file)) {
                return file;
            }
        }
    }

    /**
     * Derives a flag file for the given {@code commitFile}.
     * 
     * @param commitFile source commit file to flag
     * @return resulting result absolute path of the flag
     */
    private Path configureFlagFile(Path commitFile) {
        Objects.requireNonNull(commitFile, "commitFile must not be null");

        // take input file path with name like json_ddMMyy_hhmm[.N].txt and
        // derive its flag file name similar to json_ddMMyy_hhmm[.N].ready
        final String name = commitFile.getFileName().toString();

        final String flagName;
        final int extPos = name.lastIndexOf(".txt");
        flagName = String.format("%s.ready", extPos >= 0 ? name.substring(0, extPos) : name);

        return commitFile.resolveSibling(flagName);
    }

    /**
     * Helper function uninterruptedly waiting for a given {@code task} to complete.
     * 
     * @param task task to execute
     * @return result of the task
     * @throws ExecutionException in case of any exception raised from the task
     */
    private <V> V executeTask(Callable<V> task) throws ExecutionException {
        final Future<V> records = executor.submit(task);
        for (;;) {
            try {
                return records.get();
            } catch (final InterruptedException e) {
                Thread.interrupted();
            }
        }
    }
}
