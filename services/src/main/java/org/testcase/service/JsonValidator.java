/*
 * JsonValidator.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.service;

import java.io.IOException;
import java.io.Reader;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testcase.ValidationException;
import org.testcase.model.Subscriber;
import org.testcase.util.Json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonNodeReader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ListProcessingReport;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

/**
 * Validates {@link Subscriber}'s data against JSON schema.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
public class JsonValidator {
    private static final Logger log = LoggerFactory.getLogger(JsonValidator.class);
    private static final String SCHEMA_URI = "resource:/schema/subscriber.json";

    /** JSON schema to use for validation. */
    private JsonSchema schema;
    /** Jackson mapper we'll use for object serialization. */
    @Autowired(required = false)
    private ObjectMapper objectMapper;

    @PostConstruct
    public void postConstruct() throws ProcessingException {
        final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        schema = factory.getJsonSchema(schemaUri());

        if (objectMapper == null) {
            objectMapper = Json.createDefaultMapper();
        }
    }

    public void validate(Reader dataInput) throws ValidationException, IOException {
        Objects.requireNonNull(dataInput, "dataInput must not be null");

        try {
            final JsonNodeReader reader = new JsonNodeReader(objectMapper);
            final JsonNode data = reader.fromReader(dataInput);
            ProcessingReport report = null;
            try {
                report = schema.validate(data);

                if (!report.isSuccess()) {
                    throw new ValidationException("Validation errors found.", report);
                }

            } catch (final ProcessingException e) {
                if (log.isDebugEnabled()) {
                    log.debug("Validation failed!");
                }

                throw new ValidationException("Critical validation error.", //
                    buildReport(report, e), e);
            }
        } catch (final ValidationException e) {
            throw e;
        } catch (final Throwable e) {
            throw new ValidationException("Validation aborted due to error.", buildReport(e), e);
        }
    }

    protected String schemaUri() {
        return SCHEMA_URI;
    }

    private static ProcessingReport buildReport(ProcessingReport in, ProcessingException e) {
        final ProcessingReport out = new ListProcessingReport(LogLevel.DEBUG, LogLevel.NONE);
        try {
            out.fatal(e.getProcessingMessage());
            if (in != null) {
                out.mergeWith(in);
            }
        } catch (final ProcessingException c) {
            log.warn("Failed to build exception report.", c);
        }
        return out;
    }

    protected ProcessingReport buildReport(Throwable e) {
        final ProcessingReport out = new ListProcessingReport(LogLevel.DEBUG, LogLevel.NONE);
        try {
            final ProcessingMessage message = new ProcessingMessage();
            message.put("info", e.getMessage());
            out.fatal(message);
        } catch (final ProcessingException c) {
            log.warn("Failed to add exception details to the report.", c);
        }
        return out;
    }
}
