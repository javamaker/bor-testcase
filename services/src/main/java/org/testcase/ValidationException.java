/*
 * ValidationException.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase;

import java.util.Formatter;

import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;

/**
 * @author Ilya Yushin
 * @version $Id$
 */
public final class ValidationException extends Exception {
    private static final long serialVersionUID = 7927219283823940873L;

    private final ProcessingReport report;

    /** {@inheritDoc} */
    public ValidationException(String message, ProcessingReport report, Throwable cause) {
        super(message, cause);
        this.report = report;
    }

    /** {@inheritDoc} */
    public ValidationException(String message, ProcessingReport report) {
        super(message);
        this.report = report;
    }

    /**
     * Returns a description of the error messages.
     * 
     * @return errors description
     */
    public String getDescription() {
        if (report == null) {
            return "<empty>";
        }

        try (Formatter f = new Formatter()) {
            f.format("Status: %S", report.isSuccess() ? "success" : "failure");
            f.format("%n--- BEGIN MESSAGES ---%n");
            for (final ProcessingMessage message : report) {
                f.format("%s", message.toString());
            }
            f.format("---  END MESSAGES  ---");
            f.flush();
            return f.toString();
        }
    }
}
