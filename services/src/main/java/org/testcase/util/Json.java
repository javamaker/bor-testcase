/*
 * Json.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.util;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Helper type for easy conversion of arbitrary objects to JSON text.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
public final class Json {
    /** Creation is irrelevant. */
    private Json() {
    }

    private static final ObjectMapper json = createDefaultMapper();

    /**
     * Encodes a given <tt>source</tt> object into a JSON string.
     *
     * @param source the source object
     * @return resulting JSON string or <tt>null</tt> if the <tt>source</tt> was <tt>null</tt>
     */
    public static final String toJsonString(Object source) {
        if (source == null) {
            return null;
        }
        try {
            return json.writeValueAsString(source);
        } catch (final JsonProcessingException e) {
            return e.toString();
        }
    }

    /**
     * Creates a new {@link ObjectMapper} that can be used to pretty printing objects.
     *
     * @return resulting mapper instance
     */
    public static final ObjectMapper createDefaultMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        mapper.disable(SerializationFeature.WRITE_NULL_MAP_VALUES);
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        mapper.disable(SerializationFeature.INDENT_OUTPUT);
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
        mapper.setSerializationInclusion(Include.NON_EMPTY);
        return mapper;
    }
}
