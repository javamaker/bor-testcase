/*
 * package-info.java
 * Copyright 2017 BPC Group Banking Technologies
 */
/**
 * Utilities and helper stuff.
 * 
 * @author Ilya Yushin
 * @version $Id$
 */
package org.testcase.util;