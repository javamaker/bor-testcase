/*
 * TestJsonDirectoryStore.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase.service;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.testcase.JUnitConfiguration;
import org.testcase.model.Subscriber;
import org.testcase.util.Json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * @author Ilya Yushin
 * @version $Id$
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JUnitConfiguration.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class TestJsonDirectoryStore {
    private static final Logger log = LoggerFactory.getLogger(TestJsonDirectoryStore.class);

    @Rule
    public TestName testName = new TestName();
    @Autowired
    private JsonDirectoryStore<Subscriber> store;
    @Autowired
    private Path defaultDir;
    private ObjectMapper mapper;

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        try {
            log.info(">>>>> {}()", testName.getMethodName());
            mapper = Json.createDefaultMapper();

        } catch (final Exception e) {
            log.error("", e);
            throw e;
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        try {
            mapper = null;

        } finally {
            log.info("<<<< {}()", testName.getMethodName());
        }
    }

    @Test
    public void testGetType() {
        assertEquals(Subscriber.class, store.getType());
    }

    @Test
    public void testEnqueueRecord() throws Exception {
        try {
            final Subscriber s1 = new Subscriber();
            s1.setFirstName("A");
            s1.setLastName("B");
            s1.setMobilePhone("C");

            assertEquals(0, readQueueFile().size());
            store.enqueueRecord(s1);

            final List<Subscriber> r1 = readQueueFile();
            assertEquals(1, r1.size());
            assertEquals(s1, r1.get(0));

            store.enqueueRecord(s1);

            final Subscriber s2 = new Subscriber();
            s2.setFirstName("XXX");
            s2.setLastName("YYY");
            s2.setWorkPhone("ZZZ");
            s2.setMobilePhone("WWW");
            s2.setEmail("XX@YYY");

            store.enqueueRecord(s2);

            final List<Subscriber> r2 = readQueueFile();
            assertEquals(3, r2.size());
            assertEquals(s1, r2.get(0));
            assertEquals(s1, r2.get(1));
            assertEquals(s2, r2.get(2));

            assertEquals("XXX", r2.get(2).getFirstName());
            assertEquals("YYY", r2.get(2).getLastName());
            assertEquals("ZZZ", r2.get(2).getWorkPhone());
            assertEquals("WWW", r2.get(2).getMobilePhone());
            assertEquals("XX@YYY", r2.get(2).getEmail());

        } catch (final Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Test
    public void testQueueSize() throws Exception {
        try {
            assertEquals(0, store.queueSize());
            store.enqueueRecord(new Subscriber());
            assertEquals(1, store.queueSize());
            store.enqueueRecord(new Subscriber());
            assertEquals(2, store.queueSize());

            final List<Subscriber> r1 = readQueueFile();
            assertEquals(2, r1.size());
            assertEquals(store.queueSize(), r1.size());
            assertEquals(new Subscriber(), r1.get(0));
            assertEquals(new Subscriber(), r1.get(1));

        } catch (final Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Test
    public void testEnqueuedRecords() throws Exception {
        try {
            final Subscriber s1 = mapper.readValue("{\"firstName\":\"A\",\"lastName\":\"BBB\","
                + "\"workPhone\":null,\"mobilePhone\":\"C\"}", Subscriber.class);
            final Subscriber s2 = mapper.readValue("{\"firstName\":\"AA\",\"lastName\":\"BB\","
                + "\"mobilePhone\":\"C\",\"email\":\"test@mail.com\"}\"", Subscriber.class);
            final Subscriber s3 = mapper.readValue("{\"firstName\":\"AAA\",\"lastName\":\"B\","
                + "\"workPhone\":\"123-234-11-22\",\"mobilePhone\":\"C\"}\"", Subscriber.class);

            final List<Subscriber> r1 = store.enqueuedRecords();
            assertNotNull(r1.isEmpty());
            assertTrue(r1.isEmpty());

            store.enqueueRecord(s1);
            store.enqueueRecord(s2);
            final List<Subscriber> r2 = store.enqueuedRecords();
            assertNotNull(r2.isEmpty());
            assertFalse(r2.isEmpty());
            assertEquals(2, r2.size());
            assertTrue(r2.containsAll(Arrays.asList(s1, s2)));
            assertEquals("BBB", r2.get(0).getLastName());
            assertEquals("test@mail.com", r2.get(1).getEmail());

            store.enqueueRecord(s3);
            store.enqueueRecord(s3);
            final List<Subscriber> r3 = store.enqueuedRecords();
            assertEquals(4, r3.size());
            assertTrue(r3.containsAll(Arrays.asList(s1, s2, s3)));
            assertEquals("BBB", r3.get(0).getLastName());
            assertEquals("test@mail.com", r3.get(1).getEmail());
            assertEquals("123-234-11-22", r3.get(2).getWorkPhone());

        } catch (final Exception e) {
            log.error("", e);
            throw e;
        }
    }

    @Test
    public void testCommit() throws Exception {
        try {
            final Path workPath = defaultDir.resolve("work");
            assertTrue(Files.exists(workPath));
            assertTrue(getCommitFiles().isEmpty());

            testEnqueuedRecords();
            assertTrue(getCommitFiles().isEmpty());

            store.commit();
            final List<Path> r1 = getCommitFiles();
            assertFalse(r1.isEmpty());
            assertEquals(1, r1.size());

            final List<Subscriber> r1a = readQueueFile(r1.get(0));
            assertEquals(4, r1a.size());
            assertEquals("BBB", r1a.get(0).getLastName());
            assertEquals("test@mail.com", r1a.get(1).getEmail());
            assertEquals("123-234-11-22", r1a.get(2).getWorkPhone());

            final Subscriber[] ss = { new Subscriber(), null, new Subscriber(), new Subscriber() };
            for (int i = 0; i < 20; ++i) {
                for (int j = 0; j < ss.length; j++) {
                    store.enqueueRecord(ss[j]);
                }
                store.commit();
            }

            final List<Path> r2 = getCommitFiles();
            assertEquals(21, r2.size());
            final Subscriber expected = new Subscriber();
            int copies = 0;
            for (int i = 0; i < 21; ++i) {
                final Path p = r2.get(i);
                if (p.getFileName().toString().matches("^.+\\.\\d{1,2}\\.txt$")) {
                    copies++;
                    final List<Subscriber> r2a = readQueueFile(r2.get(i));
                    assertEquals(r2.get(i).toString(), 3 /* null had to be ignored */, r2a.size());
                    for (final Subscriber subscriber : r2a) {
                        assertEquals(r2.get(i) + " must contains blank records", expected,
                            subscriber);
                    }
                }
            }

            assertTrue(copies > 0);

            final List<Path> r3 = getFlagFiles();
            assertEquals(r2.size(), r3.size());

            // remove from the list all commit files matching found flags:
            r2.removeAll(r3.stream() //
                .map((p) -> p.toAbsolutePath().toString()) // convert to string
                .map((n) -> n.replaceAll(".ready", ".txt")) // replace extension .ready -> .txt
                .map((n) -> Paths.get(n)) // convert back to path
                .collect(Collectors.toList()));
            assertEquals(0, r2.size());

        } catch (final Exception e) {
            log.error("", e);
            throw e;
        }
    }

    //
    // Helper functions:
    //

    private List<Subscriber> readQueueFile() throws IOException {
        return readQueueFile(defaultDir.resolve("prepare/jsondata.txt"));
    }

    private List<Subscriber> readQueueFile(Path filePath) throws IOException {
        final TypeFactory factory = TypeFactory.defaultInstance();
        final CollectionType type = factory.constructCollectionType(List.class, Subscriber.class);

        if (!Files.exists(filePath)) {
            return Collections.emptyList();
        }

        return mapper.readValue(Files.readAllBytes(filePath), type);
    }

    private List<Path> getCommitFiles() throws IOException {
        final Path workPath = defaultDir.resolve("work");
        try (final DirectoryStream<Path> stream = Files.newDirectoryStream(workPath)) {
            return StreamSupport //
                .stream(stream.spliterator(), false) //
                .filter((f) -> !f.getFileName().toString().endsWith(".ready")) //
                .collect(Collectors.toList());
        }
    }

    private List<Path> getFlagFiles() throws IOException {
        final Path workPath = defaultDir.resolve("work");
        try (final DirectoryStream<Path> stream = Files.newDirectoryStream(workPath)) {
            return StreamSupport //
                .stream(stream.spliterator(), false) //
                .filter((f) -> f.getFileName().toString().endsWith(".ready")) //
                .collect(Collectors.toList());
        }
    }
}