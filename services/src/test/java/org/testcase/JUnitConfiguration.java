/*
 * JUnitConfiguration.java
 * Copyright 2017 Ilya Yushin
 */
package org.testcase;

import static org.testcase.service.JsonDirectoryLayout.*;

import java.io.Closeable;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.PropertiesPropertySource;
import org.testcase.model.Subscriber;
import org.testcase.service.JsonDirectoryLayout;
import org.testcase.service.JsonDirectoryStore;
import org.testcase.service.RecordStore;

/**
 * @author Ilya Yushin
 * @version $Id$
 */
@Configuration
public class JUnitConfiguration implements Closeable {
    private static final Logger log = LoggerFactory.getLogger(JUnitConfiguration.class);

    @Bean
    public Path defaultDir() throws IOException {
        final Path tmpDir = Files.createTempDirectory("tests");
        log.info("Creating temporary directory {}.", tmpDir);
        return tmpDir;
    }

    protected Properties testEnvironment() throws IOException {
        final Properties properties = new Properties();
        properties.setProperty(DEFAULT_DIRECTORY_PROPERTY, defaultDir().toString());
        return properties;
    }

    @Bean
    @Lazy(false)
    public Properties testEnvironment(ConfigurableEnvironment env) throws IOException {
        final Properties overriden = testEnvironment();
        final MutablePropertySources sources = env.getPropertySources();
        sources.addFirst(new PropertiesPropertySource("tests", overriden));
        return overriden;
    }

    @Bean
    public ExecutorService executor() {
        return Executors.newSingleThreadExecutor();
    }

    @Bean
    @Qualifier("recordType")
    public Class<Subscriber> recordType() {
        return Subscriber.class;
    }

    @Bean
    public JsonDirectoryLayout<Subscriber> directoryLayout() {
        return new JsonDirectoryLayout<>();
    }

    @Bean
    public RecordStore<Subscriber> store() {
        return new JsonDirectoryStore<>();
    }

    @Override
    public void close() throws IOException {
        final Path tmpDir = defaultDir();
        log.info("Deleting temporary directory {}.", tmpDir.toString());
        FileUtils.deleteQuietly(tmpDir.toFile());
    }
}
